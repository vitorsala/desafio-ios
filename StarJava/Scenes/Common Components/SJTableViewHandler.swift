//
//  SJTableViewHandler.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 24/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import UIKit

protocol SJTableViewHandlerDelegate: class {
    func didReachBottomOf(_ tableView: UITableView)
}

final class SJTableViewHandler: NSObject {
    
    fileprivate let datasource: SJTableViewDataSource
    weak var delegate: SJTableViewHandlerDelegate?
    
    var sections: [SJTableViewSection] { return self.datasource.sections }
    
    init(withData data: [SJTableViewSection], andDelegate delegate: SJTableViewHandlerDelegate? = nil) {
        self.datasource = SJTableViewDataSource(sections: data)
        self.delegate = delegate
    }
    
    func setHandler(forTableView tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self.datasource
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 64
        
        self.registerNibs(forTableView: tableView)
    }
    
    private func registerNibs(forTableView tableView: UITableView) {
        _ = self.datasource.sections.map {
            tableView.register($0.cellType.nib, forCellReuseIdentifier: $0.cellType.reuseIdentifier)
            if let header = $0.sectionHeader {
                tableView.register(header.nib, forHeaderFooterViewReuseIdentifier: header.reuseIdentifier)
            }
        }
    }
}

extension SJTableViewHandler: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.datasource.sections[indexPath.section].cellType.cellHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.datasource.sections[section].sectionHeader?.viewHeight ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerType = self.datasource.sections[section].sectionHeader else {
            return nil
        }
        
        let viewHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerType.reuseIdentifier)
        
        (viewHeader as? SJTableViewFooterHeaderBaseProtocol)?.setView(forSection: self.datasource.sections[section])
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = self.datasource.sections[indexPath.section][indexPath.item]
        let action = selectedItem.action
        tableView.deselectRow(at: indexPath, animated: UIView.areAnimationsEnabled)
        action?(selectedItem)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.section == self.datasource.sections.count - 1) && (indexPath.row == self.datasource.sections[indexPath.section].count - 1) {
            self.delegate?.didReachBottomOf(tableView)
        }
    }
}
