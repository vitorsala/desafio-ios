//
//  SJTableViewSection.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 24/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import UIKit

class SJTableViewSection {
    let title: String?
    let cellType: SJTableViewCellProtocol.Type
    let sectionHeader: SJTableViewFooterHeaderProtocol.Type?
    var items = [SJTableViewItem]()
    
    init<CT: SJTableViewCellProtocol, HT: SJTableViewFooterHeaderProtocol>(
        title: String,
        items: [SJTableViewItem],
        cellType: CT.Type,
        headerType: HT.Type)
        where CT: UITableViewCell, HT: UITableViewHeaderFooterView {
    
        self.title = title
        self.items = items
        self.cellType = cellType
        self.sectionHeader = headerType
    }
    
    init<CT: SJTableViewCellProtocol>(
        items: [SJTableViewItem],
        cellType: CT.Type)
        where CT: UITableViewCell {
        
        self.title = nil
        self.items = items
        self.cellType = cellType
        self.sectionHeader = nil
    }
}

extension SJTableViewSection {
    var count: Int { return items.count }
    
    subscript (index: Int) -> SJTableViewItem {
        return items[index]
    }
}
