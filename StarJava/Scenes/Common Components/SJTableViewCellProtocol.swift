//
//  SJTableViewCellProtocol.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 24/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import UIKit
import Reusable

typealias SJTableViewCellProtocol = SJTableViewCellBaseProtocol & NibReusable

protocol SJTableViewCellBaseProtocol {
    static var cellHeight: CGFloat { get }
    
    func setCell(forItem item: SJTableViewItem)
}

extension SJTableViewCellBaseProtocol {
    static var cellHeight: CGFloat { return UITableViewAutomaticDimension }
}

