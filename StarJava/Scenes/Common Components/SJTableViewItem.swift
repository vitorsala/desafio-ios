//
//  SJTableViewItem.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 24/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import UIKit

typealias SJTableViewItemAction = (SJTableViewItem) -> Void

class SJTableViewItem {
    
    var action: SJTableViewItemAction?
    
    init(action: SJTableViewItemAction?) {
        self.action = action
    }
}
