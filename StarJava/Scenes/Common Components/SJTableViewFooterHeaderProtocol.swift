//
//  SJTableViewFooterHeaderProtocol.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 24/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import UIKit
import Reusable

typealias SJTableViewFooterHeaderProtocol = SJTableViewFooterHeaderBaseProtocol & NibReusable

protocol SJTableViewFooterHeaderBaseProtocol {
    static var viewHeight: CGFloat { get }
    
    func setView(forSection section: SJTableViewSection)
}
