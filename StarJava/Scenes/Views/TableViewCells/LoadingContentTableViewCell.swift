//
//  LoadingContentTableViewCell.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 31/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import UIKit

class LoadingContentTableViewCell: UITableViewCell {
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView?
}

extension LoadingContentTableViewCell: SJTableViewCellProtocol {
    func setCell(forItem item: SJTableViewItem) {
        self.activityIndicator?.startAnimating()
    }
}

