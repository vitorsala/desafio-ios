//
//  RepositoryTableViewCell.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 24/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import UIKit
import Kingfisher

final class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet fileprivate weak var imgViewAvatar: UIImageView?

    @IBOutlet fileprivate weak var lblRepositoryName: UILabel?
    @IBOutlet fileprivate weak var lblRepositoryDescription: UILabel?
    @IBOutlet fileprivate weak var lblForks: UILabel?
    @IBOutlet fileprivate weak var lblStargazers: UILabel?
    
    @IBOutlet fileprivate weak var lblUsername: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupImgView()
    }
}

// MARK: - View Setups
extension RepositoryTableViewCell {
    fileprivate func setupImgView() {
        guard let imgViewAvatar = self.imgViewAvatar else { return }
        let width = imgViewAvatar.bounds.width
        
        imgViewAvatar.layer.cornerRadius = width / 2
        imgViewAvatar.layer.masksToBounds = true
    }
}

extension RepositoryTableViewCell: SJTableViewCellProtocol {
    func setCell(forItem item: SJTableViewItem) {
        guard let item = item as? RepositoryTableViewItem else {
            return
        }
        
        lblRepositoryName?.text = item.repository.name
        lblRepositoryDescription?.text = item.repository.description
        lblForks?.text = "\(item.repository.forksCount)"
        lblStargazers?.text = "\(item.repository.stargazersCount)"
        
        lblUsername?.text = item.repository.owner.name
        
        if let avatar = item.repository.owner.avatar, let url = URL(string: avatar) {
            self.imgViewAvatar?.kf.setImage(
                with: url,
                placeholder: #imageLiteral(resourceName: "DefaultProfile"))
        }
    }
}
