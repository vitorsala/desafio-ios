//
//  PullRequestTableViewCell.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 31/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import UIKit
import Kingfisher

final class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var lblTitle: UILabel?
    @IBOutlet fileprivate weak var lblBody: UILabel?
    @IBOutlet fileprivate weak var lblUserName: UILabel?
    @IBOutlet fileprivate weak var imgAvatar: UIImageView?
    
    override func awakeFromNib() {
        
        self.setupImgView()
    }
}

extension PullRequestTableViewCell {
    fileprivate func setupImgView() {
        guard let imgAvatar = self.imgAvatar else { return }
        let width = imgAvatar.bounds.width
        
        imgAvatar.layer.cornerRadius = width / 2
        imgAvatar.layer.masksToBounds = true
    }
}

extension PullRequestTableViewCell: SJTableViewCellProtocol {
    func setCell(forItem item: SJTableViewItem) {
        guard let item = item as? PullRequestTableViewItem else { return }
        self.lblTitle?.text = item.pullRequest.title
        self.lblBody?.text = item.pullRequest.body
        self.lblUserName?.text = item.pullRequest.user.name
        
        if let avatar = item.pullRequest.user.avatar, let url = URL(string: avatar) {
            self.imgAvatar?.kf.setImage(
                with: url,
                placeholder: #imageLiteral(resourceName: "DefaultProfile"))
        }
    }
}
