//
//  RepositoriesPullRequestViewController.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 31/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import UIKit
import Reusable

final class RepositoriesPullRequestViewController: UIViewController {
    
    @IBOutlet fileprivate weak var tableView: UITableView?
    fileprivate weak var tableViewRefresh: UIRefreshControl?
    fileprivate var tableViewHandler: SJTableViewHandler?
    
    var repository: Repository?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewController()
        if let tableView = self.tableView {
            self.setup(tableView: tableView)
            self.refreshData()
        }
    }
    
    fileprivate func present(errorAlert error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (_) in
            alert.dismiss(animated: UIView.areAnimationsEnabled, completion: nil)
        }
        alert.addAction(alertAction)
        self.present(alert, animated: UIView.areAnimationsEnabled, completion: nil)
    }
}

extension RepositoriesPullRequestViewController {
    fileprivate func openBrowser(url: String) {
        if let urlobj = URL(string: url) {
            UIApplication.shared.openURL(urlobj)
        } else {
            self.present(errorAlert: "Could not open the following url: \(url)")
        }
    }
}

extension RepositoriesPullRequestViewController {
    fileprivate func setupViewController() {
        self.title = repository?.name
    }
    
    fileprivate func setup(tableView: UITableView) {
        let sections = self.dataSourceSections()
        self.tableViewHandler = SJTableViewHandler(withData: sections)
        self.tableViewHandler?.setHandler(forTableView: tableView)
        self.tableView?.reloadData()
        self.setupRefresh(forTableView: tableView)
        self.tableViewRefresh?.beginRefreshing()
    }
    
    private func dataSourceSections() -> [SJTableViewSection] {
        let dataSection = SJTableViewSection(
            items: [],
            cellType: PullRequestTableViewCell.self)
        
        return [dataSection]
    }
    
    private func setupRefresh(forTableView tableView: UITableView) {
        let refresh = UIRefreshControl()
        self.tableViewRefresh = refresh
        
        refresh.addTarget(self, action: #selector(self.refreshData), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresh)
    }
}

extension RepositoriesPullRequestViewController {
    private func fetchData(completion: @escaping ([PullRequest])->Void) {
        if let repo = self.repository {
            let api = GitHubRouter.apiTarget
            api.fetchPullRequest(forRepository: repo, completion: completion) { [weak self] (error) in
                self?.present(errorAlert: error.localizedDescription)
            }
        } else {
            completion([])
        }
    }
    
    private func tableViewItem(for pullRequest: PullRequest) -> PullRequestTableViewItem {
        let pr = PullRequestTableViewItem(pullRequest: pullRequest) { [weak self] _ in
            self?.openBrowser(url: pullRequest.link)
        }
        return pr
    }
    
    fileprivate func fetchInitalData(completion: (()->Void)? = nil) {
        self.fetchData { [weak self] (pullRequests) in
            let items = pullRequests.flatMap {
                return self?.tableViewItem(for: $0)
            }
            self?.tableViewHandler?.sections[0].items = items
            self?.tableView?.reloadData()
            completion?()
        }
    }
    
    @objc
    fileprivate func refreshData() {
        fetchInitalData() {
            self.tableViewRefresh?.endRefreshing()
        }
    }
}

extension RepositoriesPullRequestViewController: StoryboardSceneBased {
    static var sceneStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle(for: RepositoriesPullRequestViewController.self))
    }
}
