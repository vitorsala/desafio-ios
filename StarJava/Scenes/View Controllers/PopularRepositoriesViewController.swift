//
//  ViewController.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 23/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import UIKit

final class PopularRepositoriesViewController: UIViewController {

    fileprivate let viewControllerTitle = "StarJava"
    fileprivate var pageIndex = 1
    
    @IBOutlet fileprivate weak var tableView: UITableView?
    fileprivate weak var tableViewRefresh: UIRefreshControl?
    fileprivate var tableViewHandler: SJTableViewHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewController()
        if let tableView = self.tableView {
            self.setup(tableView: tableView)
            self.refreshData()
        }
    }
    
    fileprivate func present(errorAlert error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (_) in
            alert.dismiss(animated: UIView.areAnimationsEnabled, completion: nil)
        }
        alert.addAction(alertAction)
        self.present(alert, animated: UIView.areAnimationsEnabled, completion: nil)
    }
}

extension PopularRepositoriesViewController {
    fileprivate func setupViewController() {
        self.title = self.viewControllerTitle
    }
    
    fileprivate func setup(tableView: UITableView) {
        let sections = self.dataSourceSections()
        self.tableViewHandler = SJTableViewHandler(withData: sections)
        self.tableViewHandler?.setHandler(forTableView: tableView)
        self.tableViewHandler?.delegate = self
        self.tableView?.reloadData()
        
        self.setupRefresh(forTableView: tableView)
        self.tableViewRefresh?.beginRefreshing()
    }
    
    private func dataSourceSections() -> [SJTableViewSection] {
        let dataSection = SJTableViewSection(
            items: [],
            cellType: RepositoryTableViewCell.self)
        let refreshSection = SJTableViewSection(
            items: [],
            cellType: LoadingContentTableViewCell.self)
        
        return [dataSection, refreshSection]
    }
    
    private func setupRefresh(forTableView tableView: UITableView) {
        let refresh = UIRefreshControl()
        self.tableViewRefresh = refresh
        
        refresh.addTarget(self, action: #selector(self.refreshData), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresh)
    }
}

extension PopularRepositoriesViewController {
    fileprivate func presentPullRequest(forRepository repository: Repository) {
        let vc = RepositoriesPullRequestViewController.instantiate()
        vc.repository = repository
        self.navigationController?.pushViewController(vc, animated: UIView.areAnimationsEnabled)
    }
}

extension PopularRepositoriesViewController {
    private func fetchData(completion: @escaping ([Repository])->Void) {
        let api = GitHubRouter.apiTarget
        api.fetchTopRepositories(forLanguage: .java, page: self.pageIndex, completion: completion) { [weak self] (error) in
            self?.present(errorAlert: error.localizedDescription)
        }
    }
    
    private func tableViewItem(for repository: Repository) -> RepositoryTableViewItem {
        
        let repo = RepositoryTableViewItem(repository: repository) { [weak self] _ in
            self?.presentPullRequest(forRepository: repository)
        }
        return repo
    }
    
    fileprivate func fetchInitalData(completion: (()->Void)? = nil) {
        self.pageIndex = 1
        self.fetchData { [weak self] (repos) in
            let items = repos.flatMap {
                return self?.tableViewItem(for: $0)
            }
            self?.tableViewHandler?.sections[0].items = items
            self?.tableViewHandler?.sections[1].items = [SJTableViewItem(action: nil)]
            self?.tableView?.reloadData()
            completion?()
        }
    }
    
    fileprivate func fetchNextPage(completion: (()->Void)? = nil) {
        self.pageIndex += 1
        self.fetchData { [weak self] (repos) in
            let items = repos.flatMap {
                return self?.tableViewItem(for: $0)
            }
            self?.tableViewHandler?.sections[0].items.append(contentsOf: items as? [SJTableViewItem] ?? [])
            self?.tableView?.reloadData()
            completion?()
        }
    }
    
    @objc
    fileprivate func refreshData() {
        fetchInitalData() {
            self.tableViewRefresh?.endRefreshing()
        }
    }
}

extension PopularRepositoriesViewController: SJTableViewHandlerDelegate {
    func didReachBottomOf(_ tableView: UITableView) {
        self.fetchNextPage()
    }
}
