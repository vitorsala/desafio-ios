//
//  Repository.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 23/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import Decodable

struct Repository {
    let id: Int
    let name: String
    let description: String?
    let stargazersCount: Int
    let forksCount: Int
    let owner: User
}

extension Repository: Decodable {
    static func decode(_ json: Any) throws -> Repository {
        return try Repository(id: json => "id",
                              name: json => "name",
                              description: json => "description",
                              stargazersCount: json => "stargazers_count",
                              forksCount: json => "forks_count",
                              owner: User.decode(json => "owner"))
    }
}
