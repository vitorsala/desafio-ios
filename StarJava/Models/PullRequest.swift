//
//  PullRequest.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 23/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import Decodable

struct PullRequest {
    let id: Int
    let title: String
    let body: String?
    let link: String
    let user: User
}

extension PullRequest: Decodable {
    static func decode(_ json: Any) throws -> PullRequest {
        return try PullRequest(id: json => "id",
                               title: json => "title",
                               body: json => "body",
                               link: json => "html_url",
                               user: User.decode(json => "user"))
    }
}
