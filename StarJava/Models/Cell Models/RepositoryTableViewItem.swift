//
//  RepositoryTableViewItem.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 24/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import UIKit

class RepositoryTableViewItem: SJTableViewItem {
    let repository: Repository
    
    init(repository: Repository, action: SJTableViewItemAction?) {
        self.repository = repository
        super.init(action: action)
    }
}
