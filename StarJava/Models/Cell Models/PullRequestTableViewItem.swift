//
//  PullRequestTableViewItem.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 31/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation

class PullRequestTableViewItem: SJTableViewItem {
    let pullRequest: PullRequest
    
    init(pullRequest: PullRequest, action: SJTableViewItemAction?) {
        self.pullRequest = pullRequest
        super.init(action: action)
    }
}
