//
//  User.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 23/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import Decodable

struct User {
    let name: String
    let avatar: String?
}

extension User: Decodable {
    static func decode(_ json: Any) throws -> User {
        return try User(name: json => "login",
                        avatar: json => "avatar_url")
    }
}
