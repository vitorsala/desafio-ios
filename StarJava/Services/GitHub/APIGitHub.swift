//
//  APIGitHub.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 23/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import Alamofire
import Decodable

enum APIError: Error {
    case invalidUrl(description: String)
    case genericError(description: String)
}

class APIGitHub {
    private let requestBuilder = URLBuilder()
    private let queue = OperationQueue()
    private let manager = SessionManager()
    
    init() {
        self.queue.maxConcurrentOperationCount = 1
    }
    
    func fetchTopRepositories(forLanguage lang: ProgrammingLanguage,
                              page: Int,
                              completion: @escaping ([Repository])->Void,
                              errorHandler: ((Error)->Void)? = nil) {
        
        self.queue.addOperation {
            guard let url = URL(string: self.requestBuilder.urlForTopRepositories(forLanguage: lang, page: page)) else {
                errorHandler?(APIError.invalidUrl(description: "Could not build URL")) ?? completion([])
                return
            }
            
            self.manager.request(url).validate().responseJSON { (response) in
                guard response.result.isSuccess,
                    let result = response.result.value as? [String: Any],
                    let items = result["items"] as? [[String: Any]] else {
                        
                        if let error = response.result.error {
                            errorHandler?(error) ?? completion([])
                        } else {
                            errorHandler?(APIError.genericError(description: "Something went wrong")) ?? completion([])
                        }
                        return
                }
                do {
                    let users = try items.flatMap { (item) -> Repository? in
                        return try Repository.decode(item)
                    }
                    completion(users)
                } catch let error {
                    errorHandler?(error) ?? completion([])
                    return
                }
            }
        }
    }
    
    func fetchPullRequest(forRepository repo: Repository,
                          completion: @escaping ([PullRequest])->Void,
                          errorHandler: ((Error)->Void)? = nil) {
        
        self.queue.addOperation {
            guard let url = URL(string: self.requestBuilder.urlForPullRequestList(forUsername: repo.owner.name, repoName: repo.name)) else {
                errorHandler?(APIError.invalidUrl(description: "Could not build URL")) ?? completion([])
                return
            }
            
            self.manager.request(url).validate().responseJSON { (response) in
                guard response.result.isSuccess,
                    let result = response.result.value as? [[String: Any]] else {
                        errorHandler?(APIError.genericError(description: "Something went wrong")) ?? completion([])
                        return
                }
                do {
                    let pullRequests = try result.flatMap { (entry) -> PullRequest? in
                        return try PullRequest.decode(entry)
                    }
                    completion(pullRequests)
                } catch let error {
                    errorHandler?(error) ?? completion([])
                }
            }
            
        }
    }
}


