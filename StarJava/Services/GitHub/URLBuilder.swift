//
//  URLBuilder.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 23/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation

enum ProgrammingLanguage: String {
    case java = "Java"
}

fileprivate enum ServiceBaseURLs: String {
    case userSearch     = "https://api.github.com/search/repositories"
    case repositoriList   = "https://api.github.com/repos/"
}

final class URLBuilder {
    
    func urlForTopRepositories(forLanguage lang: ProgrammingLanguage, page: Int) -> String {
        var url = ServiceBaseURLs.userSearch.rawValue
        url += "?q=language:\(lang.rawValue)"
        url += "&sort=stars"
        url += "&page=\(page)"
        return url
    }
    
    func urlForPullRequestList(forUsername username: String, repoName repo: String) -> String {
        var url = ServiceBaseURLs.repositoriList.rawValue
        url += username
        url += "/\(repo)"
        url += "/pulls"
        return url
    }
}
