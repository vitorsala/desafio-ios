//
//  URLBuilderSpec.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 23/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import Nimble
import Quick

@testable import StarJava

class URLBuilderSpec: QuickSpec {
    override func spec() {
        var builder: URLBuilder!
        
        beforeEach {
            builder = URLBuilder()
        }
        
        afterEach {
            builder = nil
        }
        
        describe("Builder") {
            it("should return an search url") {
                let url = builder.urlForTopRepositories(forLanguage: ProgrammingLanguage.java, page: 10)
                expect(url).to(equal("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=10"))
            }
            
            it("should return an pull request url of an given user and repositorie") {
                let url = builder.urlForPullRequestList(forUsername: "TESTE", repoName: "TESTEREPO")
                expect(url).to(equal("https://api.github.com/repos/TESTE/TESTEREPO/pulls"))
            }
        }
    }
}
