//
//  APIGitHubStub.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 23/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import Alamofire

@testable import StarJava

class APIGitHubStub: APIGitHub {
    
    var shouldFail = false
    
    private func loadJson(_ name: String) -> Any {
        
        let path = Bundle(for: APIGitHubStub.self).path(forResource: name, ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments)
        
        return json
    }
    
    override func fetchTopRepositories(forLanguage lang: ProgrammingLanguage,
                              page: Int,
                              completion: @escaping ([Repository])->Void,
                              errorHandler: ((Error)->Void)? = nil) {
        
        guard !self.shouldFail && page <= 2 else {
            errorHandler?(APIError.genericError(description: "Mocked Error"))
            return
        }
        
        let json = self.loadJson("TopStarsList\(page)") as! [String: Any]
        let items = json["items"] as! [[String: Any]]
        
        let repos = items.flatMap { (item) -> Repository? in
            do {
                return try Repository.decode(item)
            } catch {
                return nil
            }
        }
        
        completion(repos)
    }
    
    override func fetchPullRequest(forRepository repo: Repository,
                                   completion: @escaping ([PullRequest])->Void,
                                   errorHandler: ((Error)->Void)? = nil) {
        
        guard !self.shouldFail else {
            errorHandler?(APIError.genericError(description: "Mocked Error"))
            return
        }
        
        let json = self.loadJson("PullRequestsList") as! [[String: Any]]
        
        let pullRequests = json.flatMap { (entry) -> PullRequest? in
            do {
                return try PullRequest.decode(entry)
            } catch {
                return nil
            }
        }
        completion(pullRequests)
    }
}
