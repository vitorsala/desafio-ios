//
//  APIGitHubSpec.swift
//  StarJava
//
//  Created by Vitor Kawai Sala on 23/08/17.
//  Copyright © 2017 Vitor Kawai Sala. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import StarJava

class APIGitHubSpec: QuickSpec {
    override func spec() {
        var apiService: APIGitHub!
        
//        context("Online") {
//            beforeEach {
//                apiService = APIGitHub.instance
//            }
//            afterEach {
//                apiService = nil
//            }
//            describe("API") {
//                it("Must bring result for top repositories") {
//                    var results: [Repository]?
//                    apiService.fetchTopRepositories(forLanguage: .java, page: 1, completion: { (result) in
//                        results = result
//                    })
//                    expect(results).toEventuallyNot(beNil(), timeout: 5)
//                    expect(results).notTo(beEmpty())
//                    expect(results?.first?).to(beAnInstanceOf(Repository.self))
//                    expect(results?.first?.owner).to(beAnInstanceOf(User.self))
//                    
//                }
//                
//                it("Must bring result for pull requests repo 'RxJava'") {
//                    let repo = Repository(id: 7508411,
//                                          name: "RxJava",
//                                          description: "Description",
//                                          stargazersCount: 10,
//                                          forksCount: 10,
//                                          owner: User(name: "ReactiveX",
//                                                      avatar: ""))
//                    var results: [PullRequest]?
//                    apiService.fetchPullRequest(forRepository: repo, completion: { (prs) in
//                        results = prs
//                    })
//                    expect(results).toEventuallyNot(beNil(), timeout: 5)
//                    expect(results).notTo(beEmpty())
//                    expect(results?.first?).to(beAnInstanceOf(PullRequest.self))
//                    expect(results?.first?.user).to(beAnInstanceOf(User.self))
//                }
//            }
//        }
        
        context("Mocked") {
            beforeEach {
                apiService = APIGitHubStub()
            }
            
            afterEach {
                apiService = nil
            }
            
            describe("API") {
                it("Must bring result for top repositories") {
                    var results: [Repository]?
                    waitUntil(timeout: 5) { done in
                        apiService.fetchTopRepositories(forLanguage: .java, page: 1, completion: { (result) in
                            results = result
                            done()
                        })
                    }
                    expect(results).toNot(beNil())
                    expect(results).notTo(beEmpty())
                    expect(results?.first?.name).to(equal("RxJava"))
                    expect(results?.first?.stargazersCount).to(equal(26714))
                    expect(results?.first?.owner.name).to(equal("ReactiveX"))
                    
                }
                
                it("Must bring result for pull requests repo 'RxJava'") {
                    let repo = Repository(id: 7508411,
                                          name: "RxJava",
                                          description: "Description",
                                          stargazersCount: 10,
                                          forksCount: 10,
                                          owner: User(name: "ReactiveX",
                                                      avatar: ""))
                    var results: [PullRequest]?
                    apiService.fetchPullRequest(forRepository: repo, completion: { (prs) in
                        results = prs
                    })
                    expect(results).toNot(beNil())
                    expect(results).notTo(beEmpty())
                    expect(results?.first?.title).to(equal("Switch to JSR305 nullability annotations."))
                    expect(results?.first?.user.name).to(equal("artem-zinnatullin"))
                }
            }
        }
    }
}
